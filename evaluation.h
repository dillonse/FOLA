
//#IFNDEF
//#define EVALUSTION_H
#include "list_type.h"

extern int numof_succfinds;
extern int numof_memallocs;


typedef struct tag {
	
	int l_head;
	int tail_possition;
}tag;    



typedef struct tag_table{
	int size;
	tag * data;
}tag_table;

typedef struct val_store{
	int tag_lcode;
	char* idnt;
	int value;
}val_store;

typedef struct val_node* val_nodePtr;

typedef struct val_node{
	val_store data;
	val_nodePtr next;
}val_node;

typedef struct val_table{
	int size;
	val_node* data;
}val_table;


	

int add_tag(int head,int tailcode,tag_table* ListStore);
int hashhead(int list_code,tag_table ListStore);
int hashtail(int list_code,tag_table ListStore);
int hashcons(int head,int tailcode,tag_table* ListStore);
int find_val(char* idnt,int tag_pos,val_table* ValueStore,tag_table* ListStore,int* value);
int add_val(char* idnt,tag t,val_table* ValueStore,int value,tag_table* ListStore);
int eval(char* str,tag t,int is_idnt,tag_table* ListStore,val_table* ValueStore,funcPtr h);


//#endif


