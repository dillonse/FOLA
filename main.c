#include <stdlib.h>
#include <string.h>
#include "list_type.h"
#include "file_proc.h"
#include "evaluation.h"

int main(int argc, char** argv)
{
    FILE *in, *out;
    char line[256];
    int i=0, j, cond=1, wrong=0;
    funcPtr List=NULL, temp;
    ////////
    tag t;
	t.l_head=0;
	t.tail_possition=0;
	tag_table a;
	a.size=0;
	val_table b;
	b.size=0;
    /////////////////////////////////
    in=fopen("input.txt","r");
    if(in==NULL){printf("Failed opening file\n");return 0;}
    out=fopen("int_code.txt","w");
    while(feof(in)==0 && cond!=0)
    {
        if(fgets(line,sizeof line,in)==NULL)break;
        cond=process_line(line,&List,i);
        if(cond==0)
            wrong=1;
        if(cond!=2)
            i++;
    }
    if(cond==0 || wrong==1)
    {
        printf("Invalid program\n");
        clear_list_2(&List);
        fclose(in);
        fclose(out);
        return 0;
    }
    /*for each i, translate body and print stuff*/
    zero_mode(List);
    for(j=0;j<i;j++)
    {
        temp=exists_2(j,List);
        translate(temp,List);
    }
    for(j=0;j<i;j++)
    {
        temp=exists_2(j,List);
        print_stuff(out,temp);
    }
    temp=exists("result",List);
    if(temp==NULL)
    {
        printf("No result was defined");
        clear_list_2(&List);
        fclose(in);
        fclose(out);
        return 0;
    }
    //////////////EVALUATION/////////////////////
    //printf("Evaluation Segment\n");
    i=eval(temp->body,t,0,&a,&b,List);
    printf("Evaluation returned %d\n",i);
    for (i=0;i<b.size;i++)
	{
		int j;
		int list_size=0;
		val_nodePtr temp;
		temp=b.data[i].next;
		while (temp!=NULL)
		{
			temp=temp->next;
			list_size++;
		}
		for (j=0;j<list_size-1
			 ;j++)
		{
			temp=b.data[i].next;
			while (temp!=NULL)
			{
				temp=temp->next;
			}
			free(temp->data.idnt);
			temp->data.idnt=NULL;
			free(temp);
			temp=NULL;
		}
	}
	if(a.size>0)
	free(a.data);
	if(b.size>0)
	free(b.data);
	a.data=NULL;
	b.data=NULL;
	//end of evaluation//
    //////////////////////////////////
    clear_list_2(&List);
    fclose(in);
    fclose(out);
    return 0;
}
