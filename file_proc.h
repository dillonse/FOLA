#ifndef FILE_PROC_H
#define FILE_PROC_H
int condition_check(char *cond,funcPtr *h,funcPtr cur);/*returns 1 if condition is valid, else 0*/
int expression_check(char *exp,funcPtr *h,funcPtr cur);/*returns 1 if expression is valid, else 0*/
int process_line(char *line,funcPtr *h,int num);/*returns 1 if line is valid, else 0*/

////////////////////////////////////////////
/*helping functions*/
int validFunCall(char *f,funcPtr cur,funcPtr *h);/*analyzes a function call in the body of a definition*/
int validNumCall(char *n);/*valid number*/
int validVarCall(char *v);/*valid variable name*/
int simple_cond(char *cond,funcPtr *h,funcPtr cur);/*check if valid <exp><comp operator><exp>*/
#endif
