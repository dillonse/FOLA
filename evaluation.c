/*evaluation.c*/
/* all functions used for the tag implementation and the valuestore implementation,
 also the definition of the recursive
 evaluation function*/

#include "evaluation.h"
#include "list_type.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int numof_memallocs=0;
int numof_succfinds=0;

int hashhead(int list_code,tag_table ListStore)
{
	if (list_code<ListStore.size)
		return ListStore.data[list_code].l_head;
	else 
		return -1;
	
}

int hashtail(int list_code,tag_table ListStore)
{
	if(list_code<ListStore.size)
		return ListStore.data[list_code].tail_possition;
	else 
		return -1;
	
}

int hashcons(int head,int tailcode,tag_table* ListStore)
{
	int i;
	for(i=0;i<ListStore->size;i++)
		if( head==hashhead(i,*ListStore)&&tailcode==hashtail(i,*ListStore))
			return i;
	if(add_tag(head,tailcode,ListStore)!=-1)
		return ListStore->size-1;
	else{
		printf("Could not add %d %d into Liststore\n",head,tailcode);
		exit(1);
	}
}

int add_tag(int head,int tailcode,tag_table* ListStore)
{
	
	if(ListStore->size>0)
	{
		tag* temp;
		temp=realloc(ListStore->data,((ListStore->size)+1)*sizeof(tag));
		if (temp==NULL)
		{
			printf("Could not realloc ListStore\n");
			return -1;
		}
		ListStore->data=temp;
	}
	else 
	{
		ListStore->data=malloc(sizeof(tag));
	}
	
	
	
	
	ListStore->size+=1;
	(ListStore->data[ListStore->size-1]).l_head=head;
	ListStore->data[ListStore->size-1].tail_possition=tailcode;
	
	return 0;
	
}

int find_val(char* idnt,int tag_pos,val_table* ValueStore,tag_table* ListStore,int* value)
{	

	if (tag_pos<ValueStore->size)//the key exists 
	{
		val_nodePtr temp;
		temp=ValueStore->data[tag_pos].next;
		while (temp!=NULL) 
		{
			if (strcmp(idnt,temp->data.idnt)==0) 
			{
				*value=temp->data.value;
				numof_succfinds++;
				return 0;
			}
			temp=temp->next;
		}
	}
	numof_succfinds--;
	return -1;
}

int add_val(char* idnt,tag t,val_table* ValueStore,int value,tag_table* ListStore)
{
	int i=0;
	int tag_pos=hashcons(t.l_head,t.tail_possition,ListStore);
	val_node* newnode;
	if(tag_pos>=ValueStore->size)//the key did not exist
	{
		if (ValueStore->size>0) //must realloc
		{
			val_nodePtr temp;
			temp=realloc(ValueStore->data,(tag_pos+1)*sizeof(val_node));
			ValueStore->data=temp;
			temp=NULL;
			for (i=ValueStore->size;i<=tag_pos;i++) 
			{
				ValueStore->data[i].next=NULL;
				ValueStore->data[i].data.value=0;
				ValueStore->data[i].data.tag_lcode=0;
			}
		}
		else //must alloc 
		{
			ValueStore->data=malloc(sizeof(val_node)*(tag_pos+1));
			for (i=0;i<=tag_pos;i++) 
			{
				ValueStore->data[i].next=NULL;
				ValueStore->data[i].data.value=0;
				ValueStore->data[i].data.tag_lcode=0;
			}
		}
		//time to allocate for our new node to be put in list//
		newnode=malloc(sizeof(val_node));
		newnode->data.value=value;
		newnode->data.idnt=malloc(sizeof(char)*(strlen(idnt)+1));
		strcpy(newnode->data.idnt,idnt);
		newnode->data.idnt[strlen(idnt)]='\0';
		newnode->data.tag_lcode=tag_pos;
		newnode->next=NULL;
		ValueStore->data[tag_pos].next=newnode;
		ValueStore->size=tag_pos+1;
		numof_memallocs++;
		
	}
	else//the key existed
	{
		val_nodePtr temp;
		temp=ValueStore->data[tag_pos].next;
		
		while (temp!=NULL) //find apropiate place to save node in list
			temp=temp->next;
		//time to alloc new node//
		newnode=malloc(sizeof(val_node)); 
		newnode->data.value=value;
		newnode->data.idnt=malloc(sizeof(char)*(strlen(idnt)+1));
		strcpy(newnode->data.idnt,idnt);
		newnode->data.idnt[strlen(idnt)]='\0';
		newnode->data.tag_lcode=tag_pos;
		newnode->next=NULL;
		temp=newnode;
		
		
	}
	
	return 0;

}

int eval(char* str_in,tag t,int is_idnt,tag_table* ListStore,val_table* ValueStore,funcPtr h)
{
	if(is_idnt)//we have to deal with an identifier..
	{
		int u;
		char* def;
		def=getBody(str_in,h);
		u=eval(def,t,0,ListStore,ValueStore,h);
		def=NULL;
		return u;
	}
	else //we have to evaluate a definition
	{
		int j;
		int result;
		int start=0;
		int op_par=0;
		int started=0;//flag
		int ended=0;//flag
		int kill_par=0;
		int par_pos_s=0;
		int par_pos_e=0;
		char* str;
		int val_var=0;
		//check if str has parenthesis that open at first char and close at the end of str//
		
		j=0;
		while(j<strlen(str_in))
		{
			if(!started&&str_in[j]>32&&str_in[j]!='(')
			{
				kill_par=0;
				break;
			}
			if (str_in[j]=='(')
			{
				if (!started)
				{
					par_pos_s=j;
					kill_par=1;
				}
				op_par++;
				started=1;
			}
			if (str_in[j]==')')
			{
				op_par--;
				par_pos_e=j;
			}
			if (ended&&str_in[j]!=' '&&str_in[j]>31) 
			{
				kill_par=0;
				break;
			}
			if (started&&op_par==0) 
			{
				ended=1;
			}
			j++;			
		}
		
		if (kill_par)//must kill first opening and closin parenthesis//
		{
			int i;
			str=malloc(sizeof(char)*(par_pos_e-par_pos_s));
			
			for (i=0;i<par_pos_e-par_pos_s-1;i++ ) 
			{
				str[i]=str_in[i+par_pos_s+1];
			}
			str[par_pos_e-par_pos_s-1]='\0';
			result=eval(str,t,0,ListStore,ValueStore,h);
			memset(str,' ',strlen(str));
			free(str);
			str=NULL;
			return result;
		}
		else
		{
			int i;
			str=malloc(sizeof(char)*(strlen(str_in)+1));
			for (i=0;i<strlen(str_in);i++ ) 
			{
				str[i]=str_in[i];
			}
			str[strlen(str_in)]='\0';
			
		}
		
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for if E1 then E2 else E3
		{
			if(str[j]=='(')
				op_par++;
			else if(str[j]==')')
				op_par--;
			
			
			if (j<strlen(str)-4&&((str[j]==' ')||(str[j]=='('))&&str[j+1]=='i'&&str[j+2]=='f'&&str[j+3]==' '&&op_par==0)
			{
				char* temp;
				int k;
				int end;
				start=4;
				k=start;
				while(k<strlen(str)-5)
				{	
					if (str[k]==' '&&str[k+1]=='t'&&str[k+2]=='h'&&str[k+3]=='e'&&str[k+4]=='n'&&str[k+5]==' ')
					{
						end=k;
						
						
						break;
					}
					
					k++;
				}
				
				temp=malloc(sizeof(char)*(end-start+1));
				for (k=0;k<end-start;k++)
				{
					
					temp[k]=str[start+k];
				}
				temp[end-start]='\0';

				
				if (eval(temp,t,0,ListStore,ValueStore,h)==1)//case THEN
				{
					char* temp2;
					start=end+6;
					k=start;
					while (k<strlen(str)-5)
					{
						if(str[k]==' '&&str[k+1]=='e' && str[k+2]=='l'&& str[k+3]=='s'&&str[k+4]=='e'&&str[k+5]==' ')
						{
							end=k;
							break;
						}
						k++;
					}
					
					temp2=malloc(sizeof(char)*(end-start+1));
					for (k=0;k<end-start;k++)
					{
						temp2[k]=str[start+k];
					}
					temp2[end-start]='\0';
					result = eval(temp2,t,0,ListStore,ValueStore,h);
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					temp=NULL;
					temp2=NULL;
					str=NULL;
					return result;
				}
				else						//case ELSE
				{
					char* temp2;
					start=end+6;
					k=start;
					while (k<strlen(str))
					{
						if(str[k]==' '&&str[k+1]=='e' && str[k+2]=='l'&& str[k+3]=='s'&&str[k+4]=='e'&&str[k+5]==' ')
						{
							end=k;
							break;
						}
						k++;
					}
					start=end+6;
					end=strlen(str);
					temp2=malloc(sizeof(char)*(end-start+2));
					for (k=0;k<end-start;k++)
					{
						temp2[k]=str[start+k];
					}

					temp2[end-start]='\0';
					result=eval(temp2,t,0,ListStore,ValueStore,h);
					
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return result;
					
					
				}
				
				
				
			}
			
		}
		
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for not (E)
		{
			if(str[j]=='(')
				op_par++;
			else if(str[j]==')')
				op_par--;
			
			if (j<strlen(str)-4&&str[j]==' '&&str[j+1]=='n'&&str[j+2]=='o'&&str[j+3]=='t'&&str[j+4]==' '&&op_par==0) 
			{
				char*temp2;
				int k;
				temp2=malloc(sizeof(char)*(strlen(str)-5+1));
				start=5;
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';
				if(eval(temp2,t,0,ListStore,ValueStore,h)==1)
				{
					
					
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp2);
					free(str);
					str=NULL;
					temp2=NULL;
					return 0;
				}
				else 
				{
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp2);
					free(str);
					temp2=NULL;
					str=NULL;
					return 1;
				}
				
				
			}
		}
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for E1orE2
		{
			if (j<strlen(str)-3&&str[j]==' '&&str[j+1]=='o'&&str[j+2]=='r'&&str[j+3]==' '&&op_par==0) 
			{
				char*temp2;
				char*temp;
				int k;
				temp=malloc(sizeof(char)*(j-start+1));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+4;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';

				if(eval(temp2,t,0,ListStore,ValueStore,h)==1||eval(temp,t,0,ListStore,ValueStore,h)==1)
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else 
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 0;
				}
				
				
			}
		}
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for E1andE2
		{
			if (j<strlen(str)-4&&str[j]==' '&&str[j+1]=='a'&&str[j+2]=='n'&&str[j+3]=='d'&&str[j+4]==' '&&op_par==0) 
			{
				char*temp2;
				char*temp;
				int k;
				temp=malloc(sizeof(char)*(j-start+1));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+5;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';

				if(eval(temp2,t,0,ListStore,ValueStore,h)==1&&eval(temp,t,0,ListStore,ValueStore,h)==1)
				{
					
					
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else 
				{
					
					
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 0;
				}
				
				
			}
			
		}
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for E1<=||>=||<||>E2 etc...
		{
			if(str[j]=='(')
				op_par++;
			else if(str[j]==')')
				op_par--;
			
			if((str[j]=='<'||str[j]=='>') && str[j+1]=='='&&op_par==0)
			{
				char* temp;
				char*temp2;
				int k;
				temp=malloc((j-start+1)*sizeof(char));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+2;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';

				if (str[j]=='<'&&eval(temp,t,0,ListStore,ValueStore,h)<=eval(temp2,t,0,ListStore,ValueStore,h))
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else if(str[j]=='>'&&eval(temp,t,0,ListStore,ValueStore,h)>=eval(temp2,t,0,ListStore,ValueStore,h))
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else
				{	
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 0;
				}
				
				
			}
			else if ((str[j]=='<'||str[j]=='>') && str[j+1]!='='&&op_par==0)
			{
				char* temp;
				char*temp2;
				int k;
				temp=malloc((j-start+1)*sizeof(char));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+1;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';
				
				if (str[j]=='<'&&eval(temp,t,0,ListStore,ValueStore,h)<eval(temp2,t,0,ListStore,ValueStore,h))
				{	
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else if(str[j]=='>'&&eval(temp,t,0,ListStore,ValueStore,h)>eval(temp2,t,0,ListStore,ValueStore,h))
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
				else
				{	
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 0;
				}
				
			}
			else if((str[j]=='=') && str[j+1]=='='&&op_par==0)
			{
				char* temp;
				char*temp2;
				int k;
				temp=malloc((j-start+1)*sizeof(char));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+2;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';
				
				if (eval(temp,t,0,ListStore,ValueStore,h)==eval(temp2,t,0,ListStore,ValueStore,h))
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 1;
				}
			    else
				{
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					str=NULL;
					temp=NULL;
					temp2=NULL;
					return 0;
				}
				
			}
		}
		
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for E1+E2-E3 etc...
		{
			if(str[j]=='(')
				op_par++;
			else if(str[j]==')')
				op_par--;
			
			if((str[j]=='+'||str[j]=='-')&&op_par==0)
			{
				char* temp;
				char*temp2;
				int k;
				int result;
				temp=malloc((j-start+1)*sizeof(char));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+1;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';
				if (str[j]=='+')
				{
					result=eval(temp,t,0,ListStore,ValueStore,h)+eval(temp2,t,0,ListStore,ValueStore,h);
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					temp=NULL;
					temp2=NULL;
					str=NULL;
					return result;
					
				}
				else
				{	
					result=eval(temp,t,0,ListStore,ValueStore,h)-eval(temp2,t,0,ListStore,ValueStore,h);
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					temp=NULL;
					temp2=NULL;
					str=NULL;
					return result;
				}
				
				
			}
			
		}
		start=0;
		op_par=0;
		for (j=0;j<strlen(str);j++)//look for E1*E2/E3 etc...
		{
			if(str[j]=='(')
				op_par++;
			else if(str[j]==')')
				op_par--;
			
			if(str[j]=='*'||str[j]=='/'&&op_par==0)
			{
				char* temp;
				char*temp2;
				int k;
				int result;
				temp=malloc((j-start+1)*sizeof(char));
				for (k=0;k<j-start;k++)
				{
					temp[k]=str[start+k];
				}
				temp[j-start]='\0';
				start=j+1;
				temp2=malloc(sizeof(char)*(strlen(str)-start+1));
				for (k=0;k<strlen(str)-start;k++)
				{
					temp2[k]=str[start+k];
				}
				temp2[strlen(str)-start]='\0';

				if (str[j]=='*')
				{
					result=eval(temp,t,0,ListStore,ValueStore,h)*eval(temp2,t,0,ListStore,ValueStore,h);
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					temp=NULL;
					temp2=NULL;
					str=NULL;
					return result;
					
				}
				else
				{
					if(eval(temp2,t,0,ListStore,ValueStore,h)==0)
					{
						puts("ERROR::DIVISION BY ZERO");
						exit(1);
					}
					result=eval(temp,t,0,ListStore,ValueStore,h)/eval(temp2,t,0,ListStore,ValueStore,h);
					memset(temp,' ',strlen(temp)+1);
					memset(temp2,' ',strlen(temp2)+1);
					memset(str,' ',strlen(str)+1);
					free(temp);
					free(temp2);
					free(str);
					temp=NULL;
					temp2=NULL;
					str=NULL;
					return result;
				}
				
				
			}
			
		}
		
		start=0;
		op_par=0;
		j=0;
		while(j<strlen(str))//look for call_i fib etc...
		{
			if (j<strlen(str)-6 &&str[j]==' '&& str[j+1]=='c'&&str[j+2]=='a'&&str[j+3]=='l'&&str[j+4]=='l' && str[j+5]=='_') 
			{
				tag new_t;
				char* idnt;
				int k;
				int end;
				new_t.tail_possition=hashcons(t.l_head,t.tail_possition,ListStore);//calculate the new tag
				new_t.l_head=atoi(&str[j+6]);

				j+=8;
				if(new_t.l_head>0)//log10(0) is not defined in calculus ;)
				j+=log10(new_t.l_head);
				start=j-1;
				end=strlen(str);
				while (j<strlen(str))
				{
					if (str[j]==' ')
					{
						end=j-1;
						break;
					}
					j++;
				}
				idnt=malloc(sizeof(char)*(end-start+1));
				for (k=0;k<end-start;k++) 
				{
					idnt[k]=str[k+start+1];
				}
				idnt[end-start]='\0';
				memset(str,' ',strlen(str)+1);
				free(str);
				str=NULL;
				result=eval(idnt,new_t,1,ListStore,ValueStore,h);
				memset(idnt,' ',strlen(idnt)+1);
				free(idnt);
				idnt=NULL;
				
				return result;
			}
			j++;
		}
		
		
		
		start=0;
		j=0;
		op_par=0;
		val_var=0;
		if(str[0]>57||str[0]<48)//look if it is a variable like m8 K32 ara_ra9 or whatever
		{
			val_var=1;
			while (j<strlen(str))
			{
				if ((str[j]<'a')||(str[j]>'z')) 
				{
					if(str[j]>32)//no problem with control ascii 
					{
						val_var=0;
						break;
					}
					
				}
				j++;
			}
		}
		if(val_var)
		{
			int u;
			char* new_arg;
			char* new_str;
			tag new_t;
			new_arg=malloc(sizeof(char)*255);

			int p=0;
			int start_sp=0;
			int end_sp=0;
			int started=0;
			int ended=0;
			int intermid=0;
			while (p<strlen(str)) 
			{
				if (str[p]==' ') 
				{
					if(!started&&!intermid)
					{
						start_sp=p;
						started=1;
						
					}
					else if(intermid)
					{
						end_sp=p;
						ended=1;
						break;
					}
				}
				else
				{
					intermid=1;
				}
				p++;
				
			}
			if(ended) 
			{
				int tag_pos;
				new_str=malloc(sizeof(char)*(end_sp-start_sp));
				for (p=0;p<end_sp-start_sp;p++) 
				{
					new_str[p]=str[start_sp+p];
				}
				new_str[end_sp-start_sp]='\0';
				tag_pos=hashcons(t.l_head,t.tail_possition,ListStore);
				if(tag_pos<ValueStore->size)
				if (find_val(new_str,tag_pos,ValueStore,ListStore,&u)==0) //make things faster
				{
					return u;
				}
				new_arg=getAnAct(new_str,t.l_head,h); //find the actual(i,str) 
				if (new_arg==NULL)
				{
					printf("Error finding an actual\n");
					exit(1);
				}
			}
			else{
				int tag_pos;
				new_arg=getAnAct(str,t.l_head,h); //find the actual(i,str) 
				if (new_arg==NULL)
				{
					printf("Error finding an actual\n");
					exit(1);
				}
				tag_pos=hashcons(t.l_head,t.tail_possition,ListStore);
				if(tag_pos<ValueStore->size)
				if(find_val(str,tag_pos,ValueStore,ListStore,&u)==0)  //make things faster
				{
					return u;
				}

			}
			new_t.l_head=hashhead(t.tail_possition,*ListStore); //compute the new tag
			new_t.tail_possition=hashtail(t.tail_possition,*ListStore);
			result=eval(new_arg,new_t,0,ListStore,ValueStore,h); //find the value
			
			if (ended)
			{
				add_val(new_str,t,ValueStore,result,ListStore); //make things faster
				memset(new_str,' ',strlen(new_str)+1);
				free(new_str);
				new_str=NULL;

			}
			else
			{
				add_val(str,t,ValueStore,result,ListStore); //make things faster
				memset(str,' ',strlen(str)+1);
				free(str);
				str=NULL;
			}
			return result;
		}
		
		start=0;
		j=0;
		op_par=0;
		
		result=atoi(str);
		memset(str,' ',strlen(str)+1);
		free(str);
		str=NULL;
		
		return result;
		
		
		
	
		
		
		
		
	}
	
	
	
}




