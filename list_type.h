#ifndef LIST_TYPE_H
#define LIST_TYPE_H
#include <stdio.h>
struct node_0{
    char *act;/*string of an actual*/
    struct node_0* next;
};

typedef struct node_0* actPtr;

void insert_act_end(char *s,actPtr *h);/*insert new actual at end*/
char* get_actual(int i,actPtr h);/*returns pointer to ith actual, first actual with i==0*/
void clear_list_0(actPtr *h);/*deletes all elements*/
/////////////////////////////////////////////////////////////////////////
struct node_1{
    char *var;/*name of variable*/
    actPtr head;/*list with actuals of this variable*/
    struct node_1 *next;
};

typedef struct node_1* varPtr;

int num_of_vars(varPtr h);/*returns amount of variables in the list*/
int var_existence(char *s,varPtr h);/*returns 1 if var 's' exists, else 0*/
void insert_var_end(char *s,varPtr *h);/*insert new variable at end*/
void insert_act(char *a,int i,varPtr *h);/*insert actual in ith variable(create new node if ith doesnt exist)*/
void name_var(int i,char *v,varPtr h);/*name ith variable*/
char* get_actofVar(int i,char *v,varPtr h);/*get ith actual from 's' var, else NULL(if var doesnt exist)*/
void print_acts(FILE *fp,varPtr h);
void clear_list_1(varPtr *h);/*deletes all elements*/
/////////////////////////////////////////////////////////////////////////
struct node_2{
    char *name;/*name of the function*/
    char *body;/*function definition*/
    varPtr head;/*list with all the local parameters of the function*/
    int n;/*number of appearances of the function*/
    int checked;/*helps with multiple/lack of definition(s)*/
    int q;/*which number of function definition this is*/
    int params;/*amount of parameters*/
    struct node_2 *next;
};

typedef struct node_2* funcPtr;

void translate(funcPtr cur,funcPtr h);/*transform body to intensional code*/
funcPtr exists(char *s,funcPtr h);/*returns pointer to 's' if 's' is defined, else NULL*/
funcPtr exists_2(int n,funcPtr h);
char* getAnAct(char *v,int i,funcPtr h);/*returns ith actual of 'v' var of 'fun' function*/
char* getBody(char *fun,funcPtr h);/*returns body of function 'fun'*/
void clear_list_2(funcPtr *h);/*free allocated space*/
void add_func(char *s,int num,funcPtr *h,int lol);/*adds new functions with name "s" at the end*/
void add_body(char *b,funcPtr cur);
////////////////////
int get_num(char *s,funcPtr *h);/*returns number of appearances of function "s"*/
void inc_times(char *s,funcPtr *h);/*increase number of appearances*/
void zero_mode(funcPtr h);/*sets all "checked"=0 to help with translation in the end*/
void print_stuff(FILE *fp,funcPtr cur);
void translate_act(actPtr cur,funcPtr h);/*translate an actual*/
void tran_actOfVar(varPtr h,int a,funcPtr he);/*translate a-th actual of each variable*/
#endif
