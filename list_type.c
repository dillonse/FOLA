#include "list_type.h"
#include <stdlib.h>
//#include <string.h>

void insert_act_end(char *s,actPtr *h)
{
    while((*h) != NULL)
        h= &((*h)->next);
    *h= malloc(sizeof(struct node_0));
    (*h)->act= malloc((strlen(s)+1)*sizeof(char));
    strcpy((*h)->act,s);
    (*h)->next=NULL;
}

char* get_actual(int i,actPtr h)
{
    while(i!=0)
    {h=h->next;i--;}
    return h->act;
}

void clear_list_0(actPtr *h)
{
    actPtr temp;
    temp= *h;
    while(temp != NULL)
    {
        (*h)=temp->next;
        free(temp->act);
        free(temp);
        temp= *h;
    }
}
//////////////////////////////////////////////
int num_of_vars(varPtr h)
{
    int i=0;
    while(h!=NULL)
    {
        i++;
        h=h->next;
    }
    return i;
}

int var_existence(char *s,varPtr h)
{
    while(h!=NULL && strcmp(s,h->var))
        h=h->next;
    if(h!=NULL)
        return 1;
    return 0;
}

void insert_var_end(char *s,varPtr *h)
{
    while((*h) != NULL)
        h= &((*h)->next);
    *h= malloc(sizeof(struct node_1));
    (*h)->var= malloc((strlen(s)+1)*sizeof(char));
    strcpy((*h)->var,s);
    (*h)->next=NULL;
    (*h)->head=NULL;
}

void insert_act(char *a,int i, varPtr *h)
{
    while(i!=0)
    {
        h= &((*h)->next);
        i--;
    }
    if((*h)!=NULL)
        insert_act_end(a,&((*h)->head));/*ith node(variable) exists so we just insert actual*/
    else
    {
        /*must create new variable with blank var name*/
        *h= malloc(sizeof(struct node_1));
        (*h)->var=NULL;
        (*h)->next=NULL;
        (*h)->head=NULL;
        insert_act_end(a,&((*h)->head));
    }
}

void name_var(int i,char *v,varPtr h)
{
    while(i!=0)
        {h=h->next;i--;}
    h->var=malloc((strlen(v)+1)*sizeof(char));
    strcpy(h->var,v);
}

char* get_actofVar(int i,char *v,varPtr h)
{
    if(h==NULL) return NULL;
    while(h!=NULL)
    {
        if(strcmp(v,h->var)==0)
            return get_actual(i,h->head);
        else
            h=h->next;
    }
    return NULL;
}

void clear_list_1(varPtr *h)
{
    varPtr temp;
    temp= *h;
    while(temp!=NULL)
    {
        (*h)=temp->next;
        clear_list_0(&(temp->head));
        if((temp->var)!=NULL)
            free(temp->var);
        free(temp);
        temp= *h;
    }
}

void print_acts(FILE *fp,varPtr h)
{
    actPtr temp;
    temp=h->head;
    fprintf(fp,"%s = actuals(",h->var);
    while(temp!=NULL)
    {
        if(temp->next !=NULL)
            fprintf(fp,"%s,",temp->act);
        else
            fprintf(fp,"%s",temp->act);
        temp=temp->next;
    }
    fprintf(fp,")\n");
}
/////////////////////////////////////////////

funcPtr exists(char *s,funcPtr h)
{
    while(h!=NULL)
    {
        if(strcmp(s,h->name)==0)
            return h;
        h=h->next;
    }
    return NULL;
}


char* getAnAct(char *v, int i,funcPtr h)
{
    char *temp=NULL;
    while(temp==NULL && h!=NULL)
    {
        temp=get_actofVar(i,v,h->head);
        h=h->next;
    }
    if(temp==NULL)printf("weirdzzlulz\n");
    return temp;/*temp will never be NULL*/
}

char* getBody(char *fun,funcPtr h)
{
    while(strcmp(fun,h->name))
        h=h->next;
    return h->body;
}

void clear_list_2(funcPtr *h)
{
    funcPtr temp;
    temp= *h;
    while(temp!=NULL)
    {
        (*h)=temp->next;
        if((temp->name)!=NULL)
            free(temp->name);
        if((temp->body)!=NULL)
            free(temp->body);
        clear_list_1(&(temp->head));
        free(temp);
        temp= *h;
    }
}

void add_func(char *s,int num,funcPtr *h,int lol)
{
    while((*h) != NULL)
        h= &((*h)->next);
    *h = malloc(sizeof(struct node_2));
    (*h)->name=malloc((strlen(s)+1)*sizeof(char));
    strcpy((*h)->name,s);
    (*h)->n=num;
    if(num==0 ||num==-1)
        (*h)->checked=1;
    else
        (*h)->checked=0;
    (*h)->body=NULL;
    (*h)->head=NULL;
    (*h)->next=NULL;
    (*h)->q=lol;
}

//////////////////////////////////////////////
int get_num(char *s,funcPtr *h)
{
    funcPtr temp;
    temp= *h;
    while(temp!=NULL)
    {
        if(strcmp(s,temp->name)==0)
            return temp->n;
        temp=temp->next;
    }
}

void inc_times(char *s,funcPtr *h)
{
    funcPtr temp;
    temp= *h;
    while(temp!=NULL)
    {
        if(strcmp(s,temp->name)==0)
        {
            temp->n++;
            return;
        }
        temp=temp->next;
    }
}

void add_body(char *b,funcPtr cur)
{
    cur->body=malloc((strlen(b)+1)*sizeof(char));
    strcpy(cur->body,b);
}


funcPtr exists_2(int n,funcPtr h)
{
    while(h!=NULL)
    {
        if((h->q)!=n)
            h=h->next;
        else
            return h;
    }
    if(h!=NULL)
        return h;
    return NULL;
}

void zero_mode(funcPtr h)
{
    while(h!=NULL)
    {
        h->checked=0;
        h=h->next;
    }
}

void translate(funcPtr cur,funcPtr h)
{
    funcPtr pt=NULL;
    char line[256], *temp, *word_1;
    temp=cur->body;
    int i=0, j=0, y, x, k, first_point=0, second_point=0, numz, o_p, c_p;
    while(temp[i]!='\0')
    {
        while(temp[i]=='\t'||temp[i]==' ')
            i++;
        if(temp[i]=='+'||temp[i]=='-'||temp[i]=='*'||temp[i]=='/'||temp[i]=='='||temp[i]=='<'||temp[i]=='>'||temp[i]=='('||temp[i]==')')
        {
            line[j]=temp[i];
            if(temp[i]==')')
            {
                line[j+1]=' ';
                j++;
            }
            i++;
            j++;
            continue;
        }
        first_point=i;
        while(temp[i]!='\t'&&temp[i]!=' '&&temp[i]!='('&&temp[i]!=')'&&temp[i]!='\0'&&temp[i]!='+'&&temp[i]!='-'&&temp[i]!='*'&&temp[i]!='/'&&temp[i]!='>'&&temp[i]!='<'&&temp[i]!='=')
            i++;
        second_point=i-1;
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=temp[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        if(!strcmp(word_1,"and"))
        {
            line[j]=' ';
            line[j+1]='a';
            line[j+2]='n';
            line[j+3]='d';
            line[j+4]=' ';
            j+=5;
            free(word_1);
        }
        else if(!strcmp(word_1,"or"))
        {
            line[j]=' ';
            line[j+1]='o';
            line[j+2]='r';
            line[j+3]=' ';
            j+=4;
            free(word_1);
        }
        else if(!strcmp(word_1,"not"))
        {
            line[j]=' ';
            line[j+1]='n';
            line[j+2]='o';
            line[j+3]='t';
            line[j+4]=' ';
            j+=5;
            free(word_1);
            while(temp[i]=='\t'||temp[i]==' ')
                i++;
            line[j]='(';
            j++;
            i++;
        }
        else if(!strcmp(word_1,"if"))
        {
            line[j]=' ';
            line[j+1]='i';
            line[j+2]='f';
            line[j+3]=' ';
            j+=4;
            free(word_1);
        }
        else if(!strcmp(word_1,"then"))
        {
            line[j]=' ';
            line[j+1]='t';
            line[j+2]='h';
            line[j+3]='e';
            line[j+4]='n';
            line[j+5]=' ';
            j+=6;
            free(word_1);
        }
        else if(!strcmp(word_1,"else"))
        {
            line[j]=' ';
            line[j+1]='e';
            line[j+2]='l';
            line[j+3]='s';
            line[j+4]='e';
            line[j+5]=' ';
            j+=6;
            free(word_1);
        }
        else
        {
            /*variable or function name*/
            pt=exists(word_1,h);
            if(pt==NULL)
            {
                /*variable or number*/
                y=0;
                while(word_1[y]!='\0')
                {
                    line[j]=word_1[y];
                    j++;
                    y++;
                }
                line[j]=' ';
                j++;
                free(word_1);
            }
            else
            {
                /*function name*/
                numz=pt->checked;
                pt->checked++;
                tran_actOfVar(pt->head,numz,h);/*used for function calls inside other calls*/
                /*CONVERT INT TO STRING*/
                line[j]=' ';
                line[j+1]='c';
                line[j+2]='a';
                line[j+3]='l';
                line[j+4]='l';
                line[j+5]='_';
                j+=6;
                x=0;
                y=numz;
                while(y/10 != 0)
                {
                    x++;
                    y/=10;
                }
                k=x;
                while(x>=0)
                {
                    y=numz%10;
                    if(y==0)
                        line[j+x]='0';
                    else if(y==1)
                        line[j+x]='1';
                    else if(y==2)
                        line[j+x]='2';
                    else if(y==3)
                        line[j+x]='3';
                    else if(y==4)
                        line[j+x]='4';
                    else if(y==5)
                        line[j+x]='5';
                    else if(y==6)
                        line[j+x]='6';
                    else if(y==7)
                        line[j+x]='7';
                    else if(y==8)
                        line[j+x]='8';
                    else if(y==9)
                        line[j+x]='9';
                    x--;
                    numz/=10;
                }
                line[j+k+1]=' ';
                j+=k+2;
                y=0;
                while(word_1[y]!='\0')
                {
                    line[j]=word_1[y];
                    j++;
                    y++;
                }
                line[j]=' ';
                j++;
                /////////////////////////////////////
                free(word_1);
                /*ignore passing arguments*/
                o_p=c_p=0;
                while(temp[i]=='\t'||temp[i]==' ')
                    i++;
                if(temp[i]=='(')o_p++;
                else printf("Wtf weird\n");
                i++;
                while((o_p-c_p)!=0)
                {
                    if(temp[i]=='(')o_p++;
                    if(temp[i]==')')c_p++;
                    i++;
                }
            }
        }
    }
    line[j]='\0';
    /*OVERWRITE*/
    free(cur->body);
    cur->body=malloc((strlen(line)+1)*sizeof(char));
    strcpy(cur->body,line);
}


void print_stuff(FILE *fp,funcPtr cur)
{
    varPtr temp;
    fprintf(fp,"%s = %s\n",cur->name,cur->body);
    /*print actuals for each variable*/
    temp=cur->head;
    while(temp!=NULL)
    {
        print_acts(fp,temp);
        temp=temp->next;
    }
}

void tran_actOfVar(varPtr h,int a,funcPtr he)
{
    actPtr pt;
    int i;
    while(h!=NULL)/*for each variable*/
    {
        i=a;
        pt=h->head;
        while(i!=0)/*get pointer to a-th actual*/
        {
            pt=pt->next;
            i--;
        }
        translate_act(pt,he);/*translate it*/
        h=h->next;
    }
}

void translate_act(actPtr cur,funcPtr h)
{
    char line[256], *temp, *word_1;
    funcPtr pt=NULL;
    int i=0, j=0, y, x, k, first_point=0, second_point=0, numz, o_p, c_p;
    temp=cur->act;
    while(temp[i]!='\0')
    {
        while(temp[i]=='\t'||temp[i]==' ')
            i++;
        if(temp[i]=='+'||temp[i]=='-'||temp[i]=='*'||temp[i]=='/'||temp[i]=='='||temp[i]=='<'||temp[i]=='>'||temp[i]=='('||temp[i]==')')
        {
            line[j]=temp[i];
            i++;
            j++;
            continue;
        }
        first_point=i;
        while(temp[i]!='\t'&&temp[i]!=' '&&temp[i]!='('&&temp[i]!='\0'&&temp[i]!='+'&&temp[i]!='-'&&temp[i]!='*'&&temp[i]!='/'&&temp[i]!='>'&&temp[i]!='<'&&temp[i]!='=')
            i++;
        second_point=i-1;
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=temp[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        /*variable or function name*/
        pt=exists(word_1,h);
        if(pt==NULL)
        {
            /*variable or number*/
            y=0;
            while(word_1[y]!='\0')
            {
                line[j]=word_1[y];
                j++;
                y++;
            }
            line[j]=' ';
            j++;
            free(word_1);
        }
        else
        {
            /*function name*/
            numz=pt->checked;
            pt->checked++;
            tran_actOfVar(pt->head,numz,h);
            /*CONVERT INT TO STRING*/
            line[j]=' ';
            line[j+1]='c';
            line[j+2]='a';
            line[j+3]='l';
            line[j+4]='l';
            line[j+5]='_';
            j+=6;
            x=0;
            y=numz;
            while(y/10 != 0)
            {
                x++;
                y/=10;
            }
            k=x;
            while(x>=0)
            {
                y=numz%10;
                if(y==0)
                    line[j+x]='0';
                else if(y==1)
                    line[j+x]='1';
                else if(y==2)
                    line[j+x]='2';
                else if(y==3)
                    line[j+x]='3';
                else if(y==4)
                    line[j+x]='4';
                else if(y==5)
                    line[j+x]='5';
                else if(y==6)
                    line[j+x]='6';
                else if(y==7)
                    line[j+x]='7';
                else if(y==8)
                    line[j+x]='8';
                else if(y==9)
                    line[j+x]='9';
                x--;
                numz/=10;
            }
            line[j+k+1]=' ';
            j+=k+2;
            y=0;
            while(word_1[y]!='\0')
            {
                line[j]=word_1[y];
                j++;
                y++;
            }
            line[j]=' ';
            j++;
            /////////////////////////////////////
            free(word_1);
            /*ignore passing arguments*/
            o_p=c_p=0;
            while(temp[i]=='\t'||temp[i]==' ')
                i++;
            if(temp[i]=='(')o_p++;
            i++;
            while((o_p-c_p)!=0)
            {
                if(temp[i]=='(')o_p++;
                if(temp[i]==')')c_p++;
                i++;
            }
        }
    }
    line[j]='\0';
    /*OVERWRITE*/
    free(cur->act);
    cur->act=malloc((strlen(line)+1)*sizeof(char));
    strcpy(cur->act,line);
}

