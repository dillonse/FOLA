#include "list_type.h"
#include <stdlib.h>

int simple_cond(char *cond,funcPtr *h,funcPtr cur)
{
    int i=0, y, first_point=0, second_point=0;
    char *word_1;
    /*there must only be one comparison operator in the whole string*/
    while(cond[i]!='='&&cond[i]!='<'&&cond[i]!='>'&&cond[i]!='\0')
        i++;
    if(cond[i]=='\0')
        return 0;
    second_point=i-1;
    if(cond[i]=='=')
    {
        if(cond[i+1]!='=')
            return 0;
        i+=2;
    }
    else
    {
        if(cond[i+1]=='=')
            i+=2;
        else
            i++;
    }
    word_1=malloc((second_point-first_point+2)*sizeof(char));
    for(y=0;y<(second_point-first_point+1);y++)
        *(word_1+y)=cond[first_point+y];
    *(word_1+(second_point-first_point+1))='\0';
    if(!expression_check(word_1,h,cur)){free(word_1);return 0;}
    free(word_1);
    first_point=i;
    while(cond[i]!='\0')
        i++;
    second_point=i-1;
    word_1=malloc((second_point-first_point+2)*sizeof(char));
    for(y=0;y<(second_point-first_point+1);y++)
        *(word_1+y)=cond[first_point+y];
    *(word_1+(second_point-first_point+1))='\0';
    if(!expression_check(word_1,h,cur)){free(word_1);return 0;}
    free(word_1);
    return 1;
}

int validNumCall(char *n)
{
    int i=0;
    while(n[i]!='\t'&&n[i]!=' '&&n[i]!='\0')
    {
        if(n[i]<'0' || n[i]>'9')
            return 0;
        i++;
    }
    while(n[i]=='\t'||n[i]==' ')
        i++;
    if(n[i]!='\0')
        return 0;
    return 1;
}

int validVarCall(char *v)
{
    int i=0;
    /*invalid name if empty*/
    if(strlen(v)==0){printf("Invalid name\n");return 0;}
    while(v[i]!='\t'&&v[i]!=' '&&v[i]!='\0')
    {
        if(i==0)
        {
            if(*(v)<'A'||*(v)>'z'||(*(v)>'Z'&&*(v)<'a'))
                return 0;
        }
        else
        {
            if(*(v+i)>'z'||(*(v+i)<'A'&&*(v+i)>'9')||*(v+i)<'0'||(*(v+i)>'Z'&&*(v+i)<'a'&&*(v+i)!='_'))
                return 0;
        }
        i++;
    }
    while(v[i]=='\t'||v[i]==' ')
        i++;
    if(v[i]!='\0' || !strcmp(v,"and")||!strcmp(v,"or")||!strcmp(v,"not")||!strcmp(v,"if")||!strcmp(v,"then")||!strcmp(v,"else"))
        return 0;
    return 1;
}

int validFunCall(char *f,funcPtr cur,funcPtr *h)
{
    /*
    f holds the function call
    cur is a pointer to the function that is being defined right now
    *h is the head to the function list
    */
    int i=0, y, j, first_point=0, second_point=0, vars_num=1, o_p, c_p;
    char *word_1, *word_2;
    funcPtr temp;
    //////////////////
    while(f[i]=='\t'||f[i]==' ')
        i++;
    first_point=i;//check counters, possible fuckup if no function name...
    while(f[i]!='\t' && f[i]!=' ' && f[i]!='('&&f[i]!='\0')
        i++;
    second_point=i-1;
    /*saving function name in word_1*/
    word_1=malloc((second_point-first_point+2)*sizeof(char));
    for(j=0;j<(second_point-first_point+1);j++)
        *(word_1+j)=f[first_point+j];
    *(word_1+(second_point-first_point+1))='\0';
    /*check for function name validity*/
    if(validVarCall(word_1)==0){free(word_1); return 0;}
    temp=exists(word_1,*h);
    /*check if that function has already been defined*/
    if(temp==NULL)
         add_func(word_1,1,h,-1);
    else
        inc_times(word_1,h);
    while(f[i]=='\t'||f[i]==' ')
        i++;
    if(f[i]!='(')
    {printf("Invalid call of (possible)function %s\n",word_1);free(word_1);return 0;}
    first_point=i;
    o_p=1;
    c_p=0;
    while((o_p-c_p)!=0 && f[first_point]!='\0')
    {
        first_point++;
        if((o_p-c_p)==1)//possible increment of parameters(if we'r in the outter func)
            if(f[first_point]==',')
                vars_num++;
        if(f[first_point]=='(')o_p++;
        if(f[first_point]==')')c_p++;
    }
    if(temp==NULL){temp=exists(word_1,*h);temp->params=vars_num;temp=NULL;}/*helps with function inside function before function definition*/
    if(f[first_point]=='\0')
    {printf("Invalid call of (possible)function %s (missing closing parenthesis?)\n",word_1);free(word_1);return 0;}
    if(temp!=NULL)/*if function had appeared before,we compare number of parameters*/
    {
        if(temp->params!=vars_num)
        {
            printf("Invalid function call %s, conflicting number of parameters\n",word_1);
            free(word_1);
            return 0;
        }
    }
    /*each parameter must either be a number or function or one of the local parameter of the function that is being defined(or a combo of the 2)*/
    first_point=i+1;
    y=vars_num;/*'y' helps to know which parameter we are extracting(to add actual to the right )*/
    o_p=1;
    c_p=0;
    while(vars_num!=0)
    {
        while(((o_p-c_p)!=1 || f[i]!=',')&&(o_p-c_p)!=0)
        {
            i++;
            if(f[i]=='(')o_p++;
            if(f[i]==')')c_p++;
        }
        second_point=i-1;
        /*extract local parameter*/
        word_2=malloc((second_point-first_point+2)*sizeof(char));
        for(j=0;j<(second_point-first_point+1);j++)
            *(word_2+j)=f[first_point+j];
        *(word_2+(second_point-first_point+1))='\0';
        /* add actual and CHECK FOR NAME VALIDITY!!!*/
        if(temp==NULL)
            temp=exists(word_1,*h);
        insert_act(word_2,y-vars_num,&(temp->head));
        if(!expression_check(word_2,h,cur))
        {free(word_1);free(word_2);return 0;}
        free(word_2);
        word_2=NULL;
        i++;
        first_point=i;
        vars_num--;
    }
    /*rest of string must be blank*/
    while(f[i]=='\t'||f[i]==' ')
        i++;
    if(f[i]!='\0')
    {
        printf("Invalid call of (possible)function %s\n",word_1);
        free(word_1);
        return 0;
    }
    ////////////////////
    /*FREE strings*/
    free(word_1);
    return 1;
}


int expression_check(char *exp,funcPtr *h,funcPtr cur)
{
    /*o_p:opening parenthesis, c_p:closing parenthesis*/
    int i=0, y, first_point, second_point, o_p=0, c_p=0;
    char *word_1;
    ////////////////
    while(exp[i]=='\t'||exp[i]==' ')
        i++;
    first_point=i;
    if(exp[i]=='\0')
    {
        printf("Invalid expression in definition of %s\n",cur->name);
        return 0;
    }
    while(exp[i]!='\0')
    {
        while(exp[i]=='\t'||exp[i]==' ')
            i++;
        if(exp[i]=='\0')return 0;
        if(exp[i]=='(')
        {
            o_p=1;
            i++;
            first_point=i;
            while((o_p-c_p)!=0 && exp[i]!='\0')
            {
                if(exp[i]=='(')
                    o_p++;
                else if(exp[i]==')')
                    c_p++;
                i++;
            }
            if((o_p-c_p)==0)
            {
                /*allocate new expression and check for that*/
                second_point=i-2;
                /*extracting expression*/
                word_1=malloc((second_point-first_point+2)*sizeof(char));
                for(y=0;y<(second_point-first_point+1);y++)
                    *(word_1+y)=exp[first_point+y];
                *(word_1+(second_point-first_point+1))='\0';
                if(!expression_check(word_1,h,cur))
                {
                    free(word_1);
                    return 0;
                }
                free(word_1);
            }
            else
            {
                printf("Invalid expression in definition of %s\n",cur->name);
                return 0;
            }
        }
        else
        {
            /*possible function or variable*/
            first_point=i;
            while(exp[i]!='+'&&exp[i]!='-'&&exp[i]!='*'&&exp[i]!='/'&&exp[i]!='\0'||(o_p-c_p)!=0&&exp[i]!='\0')
            {
                if(exp[i]=='(')o_p++;
                if(exp[i]==')')c_p++;
                i++;
            }
            second_point=i-1;
            if(exp[i]=='+'||exp[i]=='-'||exp[i]=='*'||exp[i]=='/'||exp[i]=='\0')
                while(exp[second_point]==' '||exp[second_point]=='\t')
                    second_point--;
            /*extracting "simple" expression*/
            word_1=malloc((second_point-first_point+2)*sizeof(char));
            for(y=0;y<(second_point-first_point+1);y++)
                *(word_1+y)=exp[first_point+y];
            *(word_1+(second_point-first_point+1))='\0';
            /*check if it's num/var/func*/
            if(!validNumCall(word_1))
            {
                if(!validVarCall(word_1))
                {
                    if(!validFunCall(word_1,cur,h))
                    {
                        free(word_1);
                        printf("Invalid expression in definition\n");
                        return 0;
                    }
                }
                else
                {
                    /*if it's a variable, we also check if it exists in the local parameters of the function*/
                    if(!var_existence(word_1,cur->head))
                    {
                        free(word_1);
                        printf("Undefined variable\n");
                        return 0;
                    }
                }
            }
            free(word_1);
        }
        while(exp[i]=='\t'||exp[i]==' ')
            i++;
        if(exp[i]!='+'&&exp[i]!='-'&&exp[i]!='*'&&exp[i]!='/'&&exp[i]!='\0')
        {
            printf("Invalid expression in definition of %s\n",cur->name);
            return 0;
        }
        if(exp[i]!='\0')
        {
            i++;
            while(exp[i]=='\t'||exp[i]==' ')
                i++;
            if(exp[i]=='\0')return 0;
        }
        o_p=c_p=0;
    }
    return 1;
}


int condition_check(char *cond,funcPtr *h,funcPtr cur)
{
    int i=0, y, first_point=0, second_point=0, o_p=0, c_p=0, x, k;
    char *word_1, *word_2;
    /////////////
    while(cond[i]=='\t'||cond[i]==' ')
        i++;
    first_point=i;
    if(cond[i]=='\0')
    {printf("Invalid expression in definition of %s\n",cur->name);return 0;}
    while(cond[i]!='\0')
    {
        o_p=c_p=0;
        while(cond[i]=='\t'||cond[i]==' ')
            i++;
        if(cond[i]=='\0')return 0;
        if(cond[i]=='(')
        {
            o_p=1;
            first_point=i;
            while((o_p-c_p)!=0 && cond[i]!='\0')
            {
                i++;
                if(cond[i]=='(')o_p++;
                if(cond[i]==')')c_p++;
            }
            if(cond[i]=='\0')return 0;
            /*either big condition(all in parenthesis) or simple condition*/
            second_point=i;
            i++;
            while(cond[i]=='\t'||cond[i]==' ')
                i++;
            x=i;
            while(cond[i]!='\t'&&cond[i]!=' '&&cond[i]!='\0')
                i++;
            k=i-1;
            word_1=malloc((k-x+2)*sizeof(char));
            for(y=0;y<(k-x+1);y++)
                *(word_1+y)=cond[x+y];
            *(word_1+(k-x+1))='\0';
            /*if '\0' or "and" or "or" then big condition*/
            if((cond[i]=='\0'&&strlen(word_1)==0) || (strcmp(word_1,"and")==0&&cond[i]!='\0') || (strcmp(word_1,"or")==0&&cond[i]!='\0'))
            {
                free(word_1);
                /*big condition*/
                first_point++;
                second_point--;
                word_1=malloc((second_point-first_point+2)*sizeof(char));
                for(y=0;y<(second_point-first_point+1);y++)
                    *(word_1+y)=cond[first_point+y];
                *(word_1+(second_point-first_point+1))='\0';
                if(!condition_check(word_1,h,cur)){free(word_1);return 0;}
                free(word_1);
            }
            else
            {
                free(word_1);
                /*simple_condition*/
                /*search for next "and" or "or" or '\0'*/
                i=first_point;
                while((cond[i]!=' '&&cond[i+1]!='a'&&cond[i+2]!='n'&&cond[i+3]!='d'&&cond[i+4]!=' ')&&(cond[i+1]!=' '&&cond[i+2]!='o'&&cond[i+3]!='r'&&cond[i+4]!=' ')&&(cond[i+4]!='\0'))
                    i++;
                if(cond[i+4]=='\0')
                {
                    second_point=i+3;
                    word_1=malloc((second_point-first_point+2)*sizeof(char));
                    for(y=0;y<(second_point-first_point+1);y++)
                        *(word_1+y)=cond[first_point+y];
                    *(word_1+(second_point-first_point+1))='\0';
                    if(!simple_cond(word_1,h,cur)){free(word_1);return 0;}
                    free(word_1);
                }
                else
                {
                    second_point=i;
                    word_1=malloc((second_point-first_point+2)*sizeof(char));
                    for(y=0;y<(second_point-first_point+1);y++)
                        *(word_1+y)=cond[first_point+y];
                    *(word_1+(second_point-first_point+1))='\0';
                    if(!simple_cond(word_1,h,cur)){free(word_1);return 0;}
                    free(word_1);
                }
                i+=4;
            }
        }
        else
        {
            /*either a not(...) or a simple_condition*/
           first_point=i;
           while(cond[i]!='\t' && cond[i]!=' ' && cond[i]!='(' && cond[i]!='\0')
                    i++;
           second_point=i-1;
           word_1=malloc((second_point-first_point+2)*sizeof(char));
           for(y=0;y<(second_point-first_point+1);y++)
                *(word_1+y)=cond[first_point+y];
            *(word_1+(second_point-first_point+1))='\0';
            if(!strcmp(word_1,"not"))
            {
                while(cond[i]=='\t' || cond[i]==' ')
                    i++;
                if(cond[i]!='('){free(word_1);return 0;}
                free(word_1);
                o_p=1;
                first_point=i;
                while(cond[i]!='\0' && (o_p-c_p)!=0)
                {
                    i++;
                    if(cond[i]=='(')o_p++;
                    else if(cond[i]==')')c_p++;
                }
                if(cond[i]=='\0')return 0;
                second_point=i;
                /*save "not" contents*/
                word_1=malloc((second_point-first_point+2)*sizeof(char));
                for(y=0;y<(second_point-first_point+1);y++)
                    *(word_1+y)=cond[first_point+y];
                *(word_1+(second_point-first_point+1))='\0';
                /*continue until we find "and" or "or" or "\0"*/
                /*if we find something else, error*/
                i++;
                while(cond[i]=='\t' || cond[i]==' ')
                    i++;
                if(cond[i]=='\0')
                {
                    /*if we reached end of string then check if "not" contents are valid*/
                    if(!condition_check(word_1,h,cur)){free(word_1);return 0;}
                }
                else
                {
                    /*searching for "and", "or"*/
                    first_point=i;
                    while(cond[i]!='\t'&&cond[i]!=' '&&cond[i]!='\0')
                        i++;
                    if(cond[i]=='\0'){free(word_1);return 0;}
                    second_point=i-1;
                    word_2=malloc((second_point-first_point+2)*sizeof(char));
                    for(y=0;y<(second_point-first_point+1);y++)
                        *(word_2+y)=cond[first_point+y];
                    *(word_2+(second_point-first_point+1))='\0';
                    if(strcmp(word_2,"and")!=0 && strcmp(word_2,"or")!=0)
                    {free(word_1);free(word_2);return 0;}
                    if(!condition_check(word_1,h,cur)){free(word_1);free(word_2);return 0;}
                    free(word_1);
                    free(word_2);
                }
            }
            else
            {
                /*possible simple_condition*/
                if(cond[i]=='\0')
                    if(!simple_cond(word_1,h,cur)){free(word_1);return 0;}
                free(word_1);
                i=first_point;
                while(!(cond[i]==' '&&cond[i+1]=='a'&&cond[i+2]=='n'&&cond[i+3]=='d'&&cond[i+4]==' ')&&!(cond[i+1]==' '&&cond[i+2]=='o'&&cond[i+3]=='r'&&cond[i+4]==' ')&&!(cond[i+4]=='\0'))
                    i++;
                if(cond[i+4]=='\0')
                {
                    second_point=i+3;
                    word_1=malloc((second_point-first_point+2)*sizeof(char));
                    for(y=0;y<(second_point-first_point+1);y++)
                        *(word_1+y)=cond[first_point+y];
                    *(word_1+(second_point-first_point+1))='\0';
                    if(!simple_cond(word_1,h,cur)){free(word_1);return 0;}
                    free(word_1);
                }
                else
                {
                    second_point=i;
                    word_1=malloc((second_point-first_point+2)*sizeof(char));
                    for(y=0;y<(second_point-first_point+1);y++)
                        *(word_1+y)=cond[first_point+y];
                    *(word_1+(second_point-first_point+1))='\0';
                    if(!simple_cond(word_1,h,cur)){free(word_1);return 0;}
                    free(word_1);
                }
                i+=4;
            }
        }
    }
    return 1;
}


int process_line(char *line,funcPtr *h,int num)
{
    char *t_1="if";
    char *t_2="then";
    char *t_3="else";
    char *res="result";
    int  i=0, y=0, first_point=0, second_point=0, vars_num=1, stored_vars=0, help, c;
    char *fun_name, *word_1, *word_2;
    funcPtr temp;
    /////////////////////////////////
    while(line[i]=='\t' || line[i]==' ')/*ignore tabs and white spaces*/
        i++;
    if(line[i]=='\n' || line[i]=='\0'||line[i]=='#')return 2;
    first_point=i;
    while(line[i]!='\t'&&line[i]!=' '&&line[i]!='('&&line[i]!='\n'&&line[i]!='#'&&line[i]!='=')
        i++;
    second_point=i-1;
    /*saving string in fun_name*/
    fun_name=malloc((second_point-first_point+2)*sizeof(char));
    for(i=0;i<(second_point-first_point+1);i++)
        *(fun_name+i)=line[first_point+i];
    *(fun_name+(second_point-first_point+1))='\0';
    /*fun_name validity*/
    if(!validVarCall(fun_name))return 0;
    if(strcmp(fun_name,res)==0)
        {add_func(fun_name,-1,h,num);}
    i=second_point+1;
    while(line[i]=='\t'||line[i]==' ')
        i++;
    if(strcmp(fun_name,res)==0 && line[i]!='=')
    {
        printf("Invalid definition of result\n");
        return 0;
    }
    else if(strcmp(fun_name,res)!=0)
    {
        if(line[i]!='(')
        {printf("Invalid definition of function %s\n",fun_name);free(fun_name);return 0;}
        if((temp=exists(fun_name,*h))==NULL)
        {/*first time we find that function*/add_func(fun_name,0,h,num);stored_vars=-1;}
        else
        {
            /*that function has been found in a previous definition*/
            if(temp->checked >=1)
            {printf("Multiple definitions of function %s\n",fun_name);free(fun_name);return 0;}
            temp->checked=1;
            stored_vars=num_of_vars(temp->head);
            temp->q=num;/*store number of function*/
        }
        first_point=i+1;
        while(line[first_point]=='\t' || line[first_point]==' ')
            first_point++;
        if(line[first_point]==')')
        {printf("Absence of local parameters for function %s\n",fun_name);free(fun_name);return 0;}
        second_point=first_point;
        while(line[second_point]!=')'&&line[second_point]!='\n'&&line[second_point!='#'])
        {
            if(line[second_point]==',')
                vars_num++;
            second_point++;
        }
        if(line[second_point]=='\n'||line[second_point]=='#')
        {printf("Invalid local parameters for function %s\n",fun_name);free(fun_name);return 0;}
        /*check if amount of parameters of definition is the same with what is stored*/
        if(stored_vars!=-1 && stored_vars!=vars_num)
        {printf("Conflicting number of parameters of function %s\n",fun_name);free(fun_name);return 0;}
        /*extract each local parameter, check for name validity and name the stored variables*/
        stored_vars=vars_num;
        if(temp==NULL)
        {
            temp=exists(fun_name,*h);
            temp->params=stored_vars;
            temp=NULL;
        }
        i=first_point;
        while(vars_num!=0)
        {
            while(line[i]!=',' && line[i]!=')' &&line[i]!='\t'&&line[i]!=' ')
                i++;
            second_point=i-1;
            /*extract local parameter*/
            word_1=malloc((second_point-first_point+2)*sizeof(char));
            for(y=0;y<(second_point-first_point+1);y++)
                *(word_1+y)=line[first_point+y];
            *(word_1+(second_point-first_point+1))='\0';
            /*CHECK for name validity*/
            if(!validVarCall(word_1))return 0;
            if(temp==NULL)
            {
                temp=exists(fun_name,*h);
                insert_var_end(word_1,&(temp->head));
                temp->q=num;/*store number of function*/
                temp=NULL;
            }
            else
                {name_var(stored_vars-vars_num,word_1,temp->head);}
            free(word_1);
            word_1=NULL;
            if(line[i]==' '||line[i]=='\t')
                while(line[i]!=',' && line[i]!=')')
                    i++;
            i++;
            while(line[i]==' '||line[i]=='\t')
                i++;/*so we can have spaces before variables*/
            first_point=i;
            vars_num--;
        }
        /*continue until '='*/
        while(line[i]=='\t' || line[i]==' ')
            i++;
        if(line[i]!='=')
        {printf("Invalid definition of function %s\n",fun_name);free(fun_name);return 0;}
    }
    i++;
    if(temp==NULL)
        temp=exists(fun_name,*h);
    while(line[i]=='\t' || line[i]==' ')
        i++;
    first_point=i;
    while(line[i]!='\t'&&line[i]!=' '&&line[i]!='\n'&&line[i]!='#'&&line[i]!='(')
        i++;
    second_point=i-1;
    /*extracting string*/
    word_1=malloc((second_point-first_point+2)*sizeof(char));
    for(y=0;y<(second_point-first_point+1);y++)
        *(word_1+y)=line[first_point+y];
    *(word_1+(second_point-first_point+1))='\0';
    help=first_point;
    if(strcmp(word_1,"if")==0)
    {
        free(word_1);
        if(line[i]=='\n'||line[i]=='#')return 0;
        /*possible if-then-else*/
        first_point=i;
        c=0;
        y=i;
        while(line[y]!='\n'&&line[y]!='#')
        {
            c++;
            y++;
        }
        if(c<5){printf("Invalid If-then-else expression\n");free(fun_name);return 0;}
        while(!((line[i]==' '||line[i]==')')&&line[i+1]=='t'&&line[i+2]=='h'&&line[i+3]=='e'&&line[i+4]=='n'&&(line[i+5]==' '||line[i+5]=='('))&&line[i+5]!='\n'&&line[i+5]!='#')
            i++;
        if(line[i+5]=='\n'||line[i+5]=='#'){printf("Invalid function definition\n");free(fun_name);return 0;}
        second_point=i;
        /*check for valid condition*/
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=line[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        if(!condition_check(word_1,h,temp)){free(word_1);printf("Invalid definition\n");free(fun_name);return 0;}
        free(word_1);
        i+=5;
        first_point=i;
        c=0;
        y=i;
        while(line[y]!='\n'&&line[y]!='#')
        {
            c++;
            y++;
        }
        if(c<5){printf("Invalid If-then-else expression\n");free(fun_name);return 0;}
        while(!((line[i]==' '||line[i]==')')&&line[i+1]=='e'&&line[i+2]=='l'&&line[i+3]=='s'&&line[i+4]=='e'&&(line[i+5]==' ')||line[i+5]=='(')&&line[i+5]!='\n'&&line[i+5]!='#')
            i++;
        if(line[i+5]=='\n'||line[i+5]=='#'){printf("Invalid function definition\n");free(fun_name);return 0;}
        second_point=i;
        /*check for valid expression*/
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=line[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        if(!expression_check(word_1,h,temp)){free(word_1);printf("Invalid definition\n");free(fun_name);return 0;}
        free(word_1);
        i+=5;
        /*check the second expression*/
        first_point=i;
        while(line[i]!='\n'&&line[i]!='#')
            i++;
        second_point=i-1;
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=line[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        if(!expression_check(word_1,h,temp)){free(word_1);printf("Invalid definition\n");free(fun_name);return 0;}
        free(word_1);
        /*add body to function*/
        i=help;
        first_point=i;
        while(line[i]!='\n'&&line[i]!='#')
            i++;
        second_point=i-1;
        word_1=malloc((second_point-first_point+2)*sizeof(char));
        for(y=0;y<(second_point-first_point+1);y++)
            *(word_1+y)=line[first_point+y];
        *(word_1+(second_point-first_point+1))='\0';
        temp=exists(fun_name,*h);
        add_body(word_1,temp);
        free(word_1);
        return 1;
    }
    /*rest of string is possible "simple" expression*/
    free(word_1);
    /*save the rest of the string(after '=') and check for expression validity*/
    i=first_point;
    while(line[i]!='\n'&&line[i]!='#')
        i++;
    second_point=i-1;
    word_1=malloc((second_point-first_point+2)*sizeof(char));
    for(y=0;y<(second_point-first_point+1);y++)
        *(word_1+y)=line[first_point+y];
    *(word_1+(second_point-first_point+1))='\0';
    temp=exists(fun_name,*h);
    if(!expression_check(word_1,h,temp)){free(word_1);return 0;}
    /*add body to function*/
    temp=exists(fun_name,*h);
    add_body(word_1,temp);
    free(word_1);
    free(fun_name);
    return 1;
}

